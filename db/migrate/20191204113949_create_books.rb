class CreateBooks < ActiveRecord::Migration[5.2]
  def change
    create_table :books do |t|
      t.string :name
      t.string :writer
      t.text :description
      t.text :link

      t.timestamps
    end
  end
end
