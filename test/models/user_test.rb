require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test 'add user' do
    User.create name: 'test', email: 'test@test.test', password: 'test'
    assert_not_nil User.where(name: 'test')
  end

  test 'test validations' do
    user = User.new
    user.name = 'lol'
    user.email = 'lol'
    user.password_digest = 'lol'
    assert_equal user.save, false
  end

  def teardown
    (User.find_by name: 'test').destroy unless (User.find_by name: 'test').nil?
  end
end
