require 'test_helper'

class BookTest < ActiveSupport::TestCase
  test 'find book' do
    books = Book.all
    books.each do |b|
      if 'роман'.in? b.description
        assert_equal true, true
      else
        assert_not_equal true, false
    end
    end
  end

  test 'add book' do
    Book.create name: 'test', writer: 'test', description: 'test', link: 'test'
    assert_not_nil Book.where(name: 'test')
  end

  def teardown
    (Book.find_by name: 'test').destroy unless (Book.find_by name: 'test').nil?
  end
end
