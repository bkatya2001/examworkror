require 'test_helper'

class IntegrationTest < ActionDispatch::IntegrationTest

  test 'move to authorization' do
    get '/main'
    assert_response 302
    follow_redirect!
    assert_select 'h1', 'Вход'
  end

  test 'registration' do
    post '/user', params: {user: {name: 'test', email: 'test@test.ru', password: 'test', password_confirmation: 'test'}}
    assert_response :redirect
    follow_redirect!
    assert_select 'h1', 'Опишите, о чём Вы хотите прочитать'
  end
end
