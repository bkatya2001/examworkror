Rails.application.routes.draw do
  get '/main' => 'main#main'
  post '/main' => 'main#output'
  get '/admin' => 'main#admin'
  post '/book' => 'main#create'
  root 'sessions#new'

  get '/login' => 'sessions#new'
  post '/login' => 'sessions#create'
  get '/logout' => 'sessions#destroy'

  get '/signup' => 'user#new'
  post '/user' => 'user#create'
end
