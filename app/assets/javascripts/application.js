// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require activestorage
//= require turbolinks
//= require_tree .
function getInterestingFact() {
    let facts = ["По статистике большое количество читателей теряет интерес к книге на 18 странице",
    "В произведениях Шекспира слово «любовь» употреблялось 2259 раз",
        "Самым выпускаемым автором книг-детективов названа Агата Кристи",
        "В 2000 году вышел роман Фредерика Бегбедера '99 франков', рекомендованный к продаже " +
        "во Франции именно по такой цене. Этот же принцип послужил причиной того, что издания в " +
        "других странах выходили под другим названием, соответствующим обменному курсу: '39,90 марок' " +
        "в Германии, '9,99 фунтов' в Великобритании, '999 иен' в Японии и т.д. В 2002 году книга была " +
        "переиздана в связи с введением евро и получила название '14,99 евро'. Спустя некоторое " +
        "время пик популярности книги прошёл, и её уценили до названия и соответствующей стоимости '6 евро'",
        "Достоевский широко использовал реальную топографию Петербурга в описании мест своего романа " +
        "«Преступление и наказание». Как признался писатель, описание двора, в котором Раскольников " +
        "прячет вещи, украденные им из квартиры процентщицы, он составил из личного опыта — " +
        "когда однажды прогуливаясь по городу, Достоевский завернул в пустынный двор с целью справить нужду"];
    let num = Math.round(Math.random() * facts.length);
    if (num >= facts.length) num = facts.length - 1;
    if (num < 0) num = 0;
    document.getElementById("interestingFact").innerText = facts[num];
}