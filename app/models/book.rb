class Book < ApplicationRecord
  validates :link, presence: true, uniqueness: true
  validates :description, presence: true
  validates :name, presence: true
  validates :writer, presence: true

  def get_result(user_words)
    books = Book.all
    book = nil
    max = 0
    count = 0
    books.each do |b|
      roots = b.description.downcase.split
      roots.each do |r|
        count += 1 if r.in? user_words
      end
      if count > max
        book = b
        max = count
      end
      count = 0
    end
    result = []
    if book.nil?
      result.push('Мы не нашли подходящей литературы для вас. Попробуйте изменить параметры для повторного поиска')
    else
      result.push('Автор: ' + book.writer).push('Название: ' + book.name).push(book.link)
    end
    result
  end
end
