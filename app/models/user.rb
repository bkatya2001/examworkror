class User < ApplicationRecord
  has_secure_password

  validates :name, uniqueness: true, format: { with: /\A[a-zA-Z0-9._%+-]+\z/ }
  validates :email, uniqueness: true, format: { with: /\A[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}\z/ }
  validates :password, format: { with: /\A[a-zA-Z0-9._%+-]+\z/ }
end
