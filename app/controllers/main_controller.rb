# frozen_string_literal: true
class MainController < ApplicationController 
  before_action :authorize
  def main; end

  def admin; end

  def output
    book = Book.new
    @result = book.get_result(params[:text].downcase)
  end

  def create
    book = Book.new(book_params)
    book.save
    redirect_to '/admin'
  end

  private

  def book_params
    params.require(:book).permit(:name, :writer, :description, :link)
  end
end
