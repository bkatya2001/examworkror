class SessionsController < ApplicationController
  def new; end

  def create
    user = User.find_by_email(params[:username])
    if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      if params[:username] == 'bkatya2001@mail.ru'
        redirect_to '/admin'
      else
        redirect_to '/main'
      end
    else
      redirect_to '/login'
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to '/login'
  end
end
